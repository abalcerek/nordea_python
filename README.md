### Python exercise

1. Running python code.

    1. Run python shell with command ```python``` and evaluate string ```hello world```.
    2. Install ```ipython``` shell with command ```pip install ipython``` or use online version
    https://www.pythonanywhere.com/try-ipython/
    3. Run ipython shell with command ```ipython``` and evaluate string ```hello world```.
    
2. Arithmetic operations (use ether python shell or ipython). You can determin type of value with function ```type(value)```

    1. Calculate and check type of the result:
        1. 1 + 2
        2. 1 + 2 + 3
        3. 1.0 / 2
        4. 1.0 // 2
    2. Add all digits in current date.
    3. Write expression that returns True if current year is even. (If it is divisable without rest by 2)
    4. Get rest from division of current year by 7 as integer.
    5. Get integral part of the sum of current day and month divided by 11 as integer.

3. String operations

    1. Check type of the string ```hello world```
    2. Calculate its length
    3. Write expression that return second forth and last element of the string.
    
4. Boolean operation. 
    
    1. writes expression that:
        1. compares length of string ```hello world``` to 100 what is its value and type 
        
5. Variables

    1. Declare variable ```my_variable``` with value ```1```, evaluate it and check the type of ```m_variable``` with ```type``` function
    2. Reassign it to value ```my string value```, evaluate it and check the type of ```m_variable``` with ```type``` function
       
6. Create new project in Pycharm and create first python script that prints ```hello world```

7. Functions
    1. Create new python script with name ```functions```
    2. Write functions that prints your ```hello <your name>```
    3. Refactor it to tak name as argument and run it twice once with your name and then with the name of your neighbour.
    4. Create function that calculates sum of two numbers and returns it.
    5. Create function that returns true if number is even and false if its odd
    
8. If statements
    1. Create function ```age``` that takes number as a parameter and prints:
        1. ```old``` if age is grater then 100
        2. ```not that old``` if it less or equal then 100 but grater than 30 
        3. ```young``` if it is less or equal then 30 and is positive
        4. ```you can have negative age``` if it is less then zero
    1. Create function ``` calculate_bmi``` witch takes two parameters ```hight``` and ```weight``` and returns string 
    ```underweight```, ```proper``` or ```overweight``` based on bmi calculation.
    You can check how to calculate bmi here http://www.irishhealth.com/calc/bmi01.html
    
9. User input
    1. Create new script hello witch will take user input from the console parser it to variable name and prints 
     ```hello <name form console>``` refactor function from 6.2
    2. Refactor script bmi in the way that it will take ```height``` and ```weight``` parameters from the console input
    
10. Lists

    1. Create empty list.
    2. Create function ```empty``` that returns empty list.
    4. Create function that takes three arguments ```num1```, ```num2```, ```num3``` and returns three element list containing given parameters.
    5. Create function ```initialize_list``` that takes arguments ```len``` and ```value``` and inialize list 
       of length ```len``` with all elements equal to ```value```.
    6. Create function ```first_and_last``` that takes list as argument and returns list containing only first and last element.
    
11. Range function
    
    1. Create function that takes positive number ```N``` and returns list of integers smaller then ```N```.
    2. Create function that takes positive number ```N``` and returns list of integers smaller then ```N``` divisable by 3.
    
12. For Loops

    1. Create function that takes and list of stirings and prints all of its element.
    1. Create function that takes and list of integers and finds biggest or ```None``` if empty.
    2. Create function that takes and list of integers and finds two (or three if you want) biggest numbers or ```None``` if empty.
    3. Create function that takes and list of integers and returns sum of the values.
    4. Create function that takes and list of integers and returns sum of the values of elements on odd positions.
    5. Write the function witch count number of elements in the string.
    6. Write function that reverts the string.
    
13. While loop

    1. Create function that takes positive number ```N``` and prints all integers smaller than ```N```
    2. Create function that takes positive number ```N``` as argument and returns sum of squares of all integer smaller then ```N```
    3. Implement same program function as in previous point but use for loop and ```range``` function
    
14. Random module

    1. Create new script ```random_functions``` and import ```random``` module in to it.
    2. Create and run function that takes number ```num``` as arguemnt and returns random positive number smaller then ```num```.
    3. Create function ```roll_a_die``` witch will return random number between 1 and 6.
    4. Create function ```random_day``` that will print random day of the week. Use function ```choice``` from python ```random``` module
       Consult python documentation for details of this function https://docs.python.org/3/library/random.html
    5. Create a function random string that returns random string of length 6.
    6. Change function from point 5 to create random string of random length.
    
15. Tuples

    1. Create function that returns three element tuple with your name, surname, and age.
    2. Create a function that takes string as an argument and returns tuple with given string on the fist position and its
       length on the second.

16. Pip

    1. Install ```request``` module and ```bs4``` module.
    2. Print all installed modules. Can you find newlly installed modules?
    3. (Optional) delete requests module. Install older version of the module for example ```2.20.0``` then update it to newest version.
    
17. Virtual env

    1. Craete new directory ```my_python_env``` move to this direcotry and inside create virtual env called ```my_env```.
    2. activate ```my_env```
    3. deactivate ```my_env```
    4. (optional) activate ```my_env``` and install ```request``` module and ```bs4``` module.
    5. (optional) save all currerntly installed packages to the ```requirements.txt``` file.
    6. (optional) Delete virtual env ```my_env``` by removing ```my_env``` and recreate it. Install all packages that were stored in ```requirements.txt```
    
18. Http requests

    1. Create a function that sends ```get``` requests to the page with python documentation and prints its status and html contents.
    2. (optional) Create a function that sends ```get``` request to the address "http://api.icndb.com/jokes/random" and retrives the joke using
    ```json``` function on response object.
    
19. Web scraping (optional)
    
    1. Using request module and butiful soap module create a function that retrives all urls from pyton documentation website
    2. Using functino from point 1. Create a function that retrives 2 level nested urls from subpages and prints them.
    3. Rework function from point 2. to be able to take parameter ```depth``` of type int wich will allow you to crowl ```depth```
        levels of subpages.
    4. Modify the function that it will stay python documentation domain.
    5. Add random delays between requests. Use function ```sleep``` from python ```time``` module.
    6. Try to modify the function to be able to find and print all email addresses as well. Consult butiful soap modul documentation.
    7. Rememeber links you already visited to not crawl the same pages again.
    
20. Sets

    1. Create function that returns empty set.
    2. Create function that that takes a list as an argument and returns list without duplicates.
    3. Create function that takes a string and returns set if its letters.
    4. Create function that takes a list as argument returns true if all elements of the list are unique or false otherwise.
    5. Create function that takes a string as argument returns true if all letters of the string are unique or false otherwise.
    
21. Dict

    1. Create empty dict.
    2. Create dict with names of your colleagues as a keys and ages as value (dont have to be real).

22. Files writing

    1. Write you name to the file ```my_name.txt```
    2. Create a function that takes to strings as parameters ```file_name``` and ```content``` and creates the file with
       name ```file_name``` and writes stirng ```content``` into it.
    3. Create a function that writes ```hello``` on the first line and ```world``` on the second line.
    4. Create a function that writes 10 first natural letters in to the file. Each on new line.
    5. User file from exercise 4. and append next 10 numbers to it.

23. Files reading

    1. Use file from exercise 3. read its whole content as a string and print it.
    2. Use file from exercies 5. and print its every line.
    3. Use file from exercies 5. and sum numbers in each line and print it.
    
24. Files other operations
    
    1. List all files in current directory
    2. Create a new directory in current directory and move one of previously created files in to it.
    3. Delete one of the previously created files.
    
25. Object

    1. Create class ```Dog``` with fields ```name``` and ```age```. Instantiate it and print it.
    2. Create method ```bark``` that prints ```woof woof```
    3. Create ```__str__``` that returns readable string when objects is printed.
    4. Add docstring under class and method definision.

26. List comprehensions

    1. Create a function that takes a list of integers as argument and returns list with added 2 to each element of the original one.
    2. Create a function that takes a list of strings as argument and returns list in witch every word from original list is capitalized.
    3. (optitnal) Create a function that takes integer ```N``` as argument and returns list of pairs of integers (i, j) where 0 < i, j < ```N```. 

    

